'''
Created on May 31, 2018

@author: Lucian
'''
import os

from book import book
from fuzzywuzzy import fuzz, process

import csv
import pickle
import string
codeList=os.listdir("nongoogleCombined")
bookList=[]
count=0
strictWL=string.ascii_letters+"\'"
whiteList=string.ascii_letters+ string.digits+ " "+"."+"\n"+"\'"
prefaceStart="ever private man"
novelStart = "born year 1632, city York"
novelEnd="farther account years part"
endText="the end"
secondStart="Further adventures"
secondLine="homely proverb England bone"
count = 0
endCount=0
bookDict={}

with open("booksFinal.txt", "rb") as fp:
    bookDict=pickle.load(fp)
print(len(bookDict))
for code in bookDict:
    textList=bookDict[code].text.split('\n')
    endLoc=len(bookDict[code].text)
    endStr=""
    for part in textList:
        endScore=fuzz.token_set_ratio(secondLine, part)
        
        if endScore>70 and len(part)>20 :
            endLoc=bookDict[code].text.find(part)
            endStr=part
            endCount+=1
            
    if endLoc/(len(bookDict[code].text)+1)>0.4:
        print(endStr+" "+bookDict[code].code+" "+str(endLoc/len(bookDict[code].text)))
        bookDict[code].hasBook2=True        
        bookDict[code].text=bookDict[code].text[:endLoc]
    for part in textList:
        
        prefaceScore = fuzz.token_set_ratio(prefaceStart, part)
        novelScore = fuzz.token_set_ratio(novelStart, part)
        if prefaceScore > 70 and len(part) > 12:
            bookDict[code].hasPreface=True
            '''
            startLoc=book.text.find(part)
            ##print(part+" "+book.code+" "+str(prefaceScore))
            book.text=book.text[startLoc:]
            count+=1
            break
            '''
        elif novelScore> 60 and len(part) > 12:
            startLoc=bookDict[code].text.find(part)
            ##print(part+" "+book.code+" "+str(novelScore))
            bookDict[code].text=bookDict[code].text[startLoc:]
            count+=1
            break
        
print(count)
print(endCount)
with open("booksFinal.txt", "wb") as fp:   #Pickling
    pickle.dump(bookDict, fp)

