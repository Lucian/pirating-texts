'''
Created on May 31, 2018

@author: Lucian
'''
import code

class book:
    '''
    
    '''


    def __init__(self, title, date, language, code, lat, long, oclc, country, text): #lat, long, oclc):
        '''
        Constructor
        '''
        self.title=title
        self.date=date
        
        self.language=language
        self.code=code
        self.text=text
        self.lat=lat
        self.long=long
        self.oclc=oclc
        self.country=country
        self.segList=[]
        self.sentList=[]
        self.hasPreface=False
        self.hasJournal=False
        self.hasEvilGood=False
        self.hasMaster=False
        self.hasFoot=False
        self.hasMorocco=False
        self.hasDream=False
        self.hasSpain=False
        self.hasBook2=False
        self.hasPlant=False
    def printMe(self):
        print("Title: "+self.title)
        print("Code: "+ self.code)
        print("Language: "+self.language)