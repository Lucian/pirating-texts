


import glob
import os
import codecs
from os import listdir
from fuzzywuzzy import fuzz, process

##your file location of unzipped folders
upperNames = os.listdir("unzipped2")
print(upperNames)
for fileName in upperNames:
    filenames=os.listdir("unzipped2/"+fileName+"//"+fileName.split(".")[1])
    firstLineList=[]
    
    headerStatus=False
    
    ##file location where you want to place the concatenated txt files
    with open("nongoogleCombined/"+fileName+".txt", 'w') as outfile:
        for fname in filenames:
            print(fileName+" "+fname)
            fullName="unzipped2/"+fileName+"//"+fileName.split(".")[1]+"//"+fname
            
            with codecs.open(fullName, "r",encoding='utf-8', errors='ignore') as infile:
                lineNum=0
                
                for line in infile:
                    
                    if(lineNum==0):
                        for firstLine in firstLineList:
                            if(fuzz.ratio(firstLine,line)>60 and len(line)>5 and headerStatus==False):
                                headerStatus=True
                                ##print("possible duplicate "+line)
                                break
                           
                        
                    try:
                        if(lineNum==0):
                            if(headerStatus==False):
                                outfile.write(line)
                            else:
                                repeat=False
                                for firstLine in firstLineList:
                                    if(fuzz.ratio(line,firstLine)>70):
                                        repeat=True
                                        print(line+" "+firstLine)
                                        break
                                if(repeat==False):
                                    outfile.write(line)
                                else:
                                    print("deleted line "+line+" ")
                        else:
                            outfile.write(line)
                    except UnicodeEncodeError:
                        if(lineNum==0):
                            if(headerStatus==False):
                                charList=list(line)
                                
                                for char in charList:
                                    try:
                                       outfile.write(char) 
                                    except UnicodeEncodeError:
                                       pass
                                pass
                        else:
                            charList=list(line)
                            for char in charList:
                                try:
                                    outfile.write(char) 
                                except UnicodeEncodeError:
                                    pass
                                pass
                    if(len(line)>1):
                        lineNum+=1
                    firstLineList.append(line)