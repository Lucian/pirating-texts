'''
Created on Jun 20, 2018

@author: Lucian
'''
from book import book 
import pickle
import numpy
bookList=[]
with open("bookAbridged.txt", "rb") as fp:
        bookList=pickle.load(fp)
sortedList=sorted(bookList,key=lambda book: book.date)
count=0
begDate=""
endDate=""
currentScenes=[0]*8
print(currentScenes)
sceneMatrix=[]
print(sortedList)
for index in sortedList:
    print(count)
    if count==0:
        begDate=index.date
    if count==10:
        endDate=index.date
        count=0
        
        currentScenes.append(begDate+"-"+endDate)
        sceneMatrix.append(currentScenes)
        begDate=index.date
        currentScenes=[0]*7
    if index.hasMorocco:
        currentScenes[0]+=1
    if index.hasEvilGood:
        currentScenes[1]+=1
    if index.hasJournal:
        currentScenes[2]+=1
    if index.hasDream:
        currentScenes[3]+=1
    if index.hasFoot:
        currentScenes[4]+=1
    if index.hasMaster:
        currentScenes[5]+=1
    if index.hasSpain:
        currentScenes[6]+=1
    count+=1
print(numpy.matrix(sceneMatrix))

    