'''
Created on Jun 26, 2018

@author: Lucian
'''
import nltk
import string
import pickle
result=[]
bookList=[]
try:
    with open("list.txt", "rb") as fp:
            result=pickle.load(fp)
    print(len(result))
    with open("bookAbdridged.txt", "rb") as fp:
        bookList=pickle.load(fp)
    
except:
    whiteList=string.ascii_letters+ string.digits+ " "+"."+"\'"+"\n"
    with open("canonical.txt",encoding='utf-8', errors='ignore') as myfile:
        text=myfile.read()
    text = ''.join(c for c in text if c in whiteList)
    text=text.lower()  
    ttt = nltk.tokenize.TextTilingTokenizer()
    result=ttt.tokenize(text)    
    with open("list.txt", "wb") as fp:   #Pickling
        pickle.dump(result, fp)