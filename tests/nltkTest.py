'''
Created on Jun 11, 2018

@author: Lucian
'''
import nltk
from nltk import bigrams
from nltk import ne_chunk, pos_tag, word_tokenize
from nltk.tree import Tree
import pickle
from book import book
import string
import math
from nltk import NgramModel
from nltk.util import ngrams
from nltk.probability import LidstoneProbDist
bookList=[]
with open("bookAbdridged.txt", "rb") as fp:
        bookList=pickle.load(fp)
    
def lexical_diversity(text):
    return len(set(text)) / len(text)
    
def unusual_words(text):
    text_vocab = set(w.lower() for w in text if w.isalpha())
    english_vocab = set(w.lower() for w in nltk.corpus.words.words())
    unusual = text_vocab - english_vocab
    return sorted(unusual)
def ie_preprocess(document):
    sentences=nltk.sent_tokenize(document)
    #sentences = [nltk.word_tokenize(sent) for sent in sentences]
    #sentences = [nltk.pos_tag(sent) for sent in sentences]
    return sentences
def get_continuous_chunks(text):
     chunked = text
     prev = None
     continuous_chunk = []
     current_chunk = []
     for i in chunked:
             if type(i) == Tree:
                     current_chunk.append(" ".join([token for token, pos in i.leaves()]))
             elif current_chunk:
                     named_entity = " ".join(current_chunk)
                     if named_entity not in continuous_chunk:
                             continuous_chunk.append(named_entity)
                             current_chunk = []
             else:
                     continue
     return continuous_chunk
    
with open('canonical.txt', 'r', encoding='utf-8', errors='ignore') as myfile:
    text = myfile.read()
whiteList=string.ascii_letters+ string.digits+ " "+"."+"\n"+"\'"
text = ''.join(c if c in whiteList else " " for c in text )

tokens = nltk.word_tokenize(text)
bigram = trigrams(tokens)
print(*map(' '.join, bigram), sep=', ')
'''
neList=set([])
for sent in nltk.sent_tokenize(bookList[0].text):
   for chunk in nltk.ne_chunk(nltk.pos_tag(nltk.word_tokenize(sent))):
      if hasattr(chunk, 'label'):
         print(chunk.label(), ' '.join(c[0] for c in chunk))
         a=str(chunk.label())
         b=str(' '.join(c[0] for c in chunk))
         neList.add(a+" "+b)
for string in neList:
    print(string)
'''         