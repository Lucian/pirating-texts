'''
Created on Jun 21, 2018

@author: Lucian
'''
import uts
import string
import pickle
from book import book
from fuzzywuzzy import fuzz

text=""
whiteList=string.ascii_letters+ string.digits+ " "+"."+"\'"
bookList=[]
transitionList=[]
with open("canonical.txt",encoding='utf-8', errors='ignore') as myfile:
    text=myfile.read()
with open("bookAbdridged.txt", "rb") as fp:
    bookList=pickle.load(fp)
text = ''.join(c for c in text if c in whiteList)
text=text.lower()  
words=[text[i:i + int(len(text)/100)] for i in range(0, len(text), int(len(text)/100))]  
model = uts.TextTiling(window=2)
boundary = model.segment(words)
canonicalTrans=[]
thefile = open('list.txt', 'w')
for x in range(1,len(words)):
    if(boundary[x]==1):
        canonicalTrans.append(words[x])
        thefile.write("%s\n" % words[x])
print(canonicalTrans[4])
'''
for book in bookList:
    text=book.text    
    text = ''.join(c for c in text if c in whiteList)
    text=text.lower()  
    words=[text[i:i + int(len(text)/100)] for i in range(0, len(text), int(len(text)/100))]  
    model = uts.TextTiling(window=2)
    boundary = model.segment(words)
    trans=[]
    for x in range(1,len(words)):
        if(boundary[x]==1):
            trans.append(words[x])
            
    transitionList.append(trans)
unity=[0]*100
thefile = open('list.txt', 'w')

for terms in transitionList:
    for words in terms:
        for x in range(0,len(canonicalTrans)):
            if fuzz.token_set_ratio(canonicalTrans[x],words)>80:
                thefile.write("%s\n" % words)
                
                print(words)
                unity[x]+=1
print(unity)
'''