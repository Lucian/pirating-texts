'''
Created on Jun 12, 2018

@author: Lucian
'''
# -*- coding: utf-8 -*-
import requests
import json
import pandas
import codecs

responses = []

for i in range(1,6):
    title = "Robinson Crusoe"
    params = {'title': title, 'page' : i}
    response = requests.get('http://openlibrary.org/search.json', params=params)
    responses.append(response)


parsedJSON = [json.loads(r.text) for r in responses]

crusoe_dict = parsedJSON[0]




alltitles = []
allpubp = []
allpub = []
years =[]
dates = []



for doc in crusoe_dict['docs']: 
    print(str(doc["publish_place"]))
    print(str(doc["publish_year"]))
    print(str(doc["publisher"]))
    if 'title' in doc:
        
        if isinstance(doc['title'], list):
            alltitles = alltitles + doc['title']
        else:
            alltitles.append(doc['title'])
        
    if 'publish_place' in doc:
        if isinstance(doc['publish_place'], list):
            allpubp = allpubp + doc['publish_place']
        else:
            allpubp.append(doc['publish_place'])

    if 'publisher' in doc:
        if isinstance(doc['publisher'], list):
            allpub = allpub + doc['publisher']
        else:
            allpub.append(doc['publisher'])

    if 'publish_year' in doc:
        if isinstance(doc['publish_year'], list):
            years = years + doc['publish_year']
        else:
            years.append(doc['publish_year'])

    if 'publish_date' in doc:
        if isinstance(doc['publish_date'], list):
            dates = dates + doc['publish_date']
        else:
            dates.append(doc['publish_date'])

df = pandas.DataFrame([alltitles, allpub, allpubp, years, dates] , index = ['title', 'publisher', 'publish_place', 'publish_date', 'publish_year'])
df =df.T
df.to_csv('crusoe.csv', sep=',', encoding='utf-8')
