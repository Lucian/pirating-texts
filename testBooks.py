'''
Created on Jun 6, 2018

@author: Lucian
'''
import pickle
from book import book
with open("books.txt", "rb") as fp:
        bookList=pickle.load(fp)
for book in bookList:
    print(book.code+" "+book.language)