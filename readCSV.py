'''
Created on May 31, 2018

@author: Lucian
'''
import os

from book import book
from fuzzywuzzy import fuzz, process

import csv
import pickle
import string
codeList=os.listdir("nongoogleCombined")
bookList=[]
count=0
strictWL=string.ascii_letters+"\'"
whiteList=string.ascii_letters+ string.digits+ " "+"."+"\n"+"\'"
prefaceStart="ever private man"
novelStart = "born year 1632, city York"
novelEnd="farther account years part"
endText="the end"
secondStart="Further adventures"
secondLine="homely proverb England bone"
count = 0
endCount=0
bookDict={}

#from enchant import DictWithPWL
#from enchant.checker import SpellChecker
text=""
with open("canonical.txt",encoding='utf-8', errors='ignore') as myfile:
    try:
        for line in myfile:
            text+=str(line)
    except UnicodeDecodeError:
        for line in myfile:
            charList=list(line)
                            	
            for char in charList:
                try:
                    text+=str(char)
                except UnicodeEncodeError:
                    text+=" "
                    pass
                pass
            
text=text.replace("-", ' ')

text=text.replace("\n", ' ')
bookList.append(book("gutenberg", "1919", "eng", "gutenberg", text, "51.50732", "-0.12765", "gutenberg", "GB"))
with open("first_ed.txt",encoding='utf-8', errors='ignore') as myfile:
    text=myfile.read()
text=text.replace("-", ' ')
text=text.replace("\n", ' ')
bookList.append(book("first_ed", "1719", "eng", "first_ed", text, "51.50732", "-0.12765", "first_ed", "GB"))

for code in codeList:
    codeFirst=code.split(".txt")[0]
    print(codeFirst)
    codeFirst=codeFirst.replace("+",":")
    codeFirst=codeFirst.replace("=","/")
    with open("nongoogleCombined/"+code, encoding='utf-8', errors='ignore') as myfile:
        with open("crusoeMaster1.csv", 'r',encoding='utf-8', ) as csvfile,  open('master.csv', 'w',encoding='utf-8') as out:
            writer = csv.writer(out,lineterminator = '\n')
            reader=csv.reader(csvfile, delimiter=',', quotechar='\"')
            for row in reader:
                count+=1
                
                csvCode=row[0].replace("\"", "")
                
                if csvCode==codeFirst:
                    row[6]=1
                    title=row[3].replace("\"", "")
                    date=row[2].replace("\"", "")
                    language=row[5].replace("\"", "")
                    lat=row[7].replace("\"", "")
                    long=row[8].replace("\"", "")
                    oclc=row[4].replace("\"", "")
                    try:
                        country=row[9]
                    except:
                        print("not found")
                writer.writerow(row)
        
        data = myfile.read()
        d = {}
        final = ' '
        count = 0

        with open('CorrectionRules.txt', 'r', encoding='utf-8') as myfile:
            fixes = myfile.read()
            lst = fixes.split('\n')

            for item in lst:
                x = item.split()
                d[x[0]] = x[1]

        fix=data.split(" ")
        for x, word in enumerate(fix):
            if word in d:
                count+=1
                fix[x] = d[word]
        print(count)
        fixed=' '.join(fix)
        data = ''.join(c if c in whiteList else " " for c in fixed)
        #print(fixed)
        #checker.set_text(data)
        '''
        for error in checker:
            for suggestion in error.suggest():
                if error.word.replace(' ', '') == suggestion.replace(' ', ''):  # make sure the suggestion has exact same characters as error in the same order as error and without considering spaces
                    ##print(error.word+" "+suggestion)
                    error.replace(suggestion)
                    break
        #data=checker.get_text()
        '''
        currentBook=book(title, date, language, code, lat, long, oclc, country, data)
        bookList.append(currentBook)
        currentBook.printMe()
for book in bookList:
    bookDict[book.code]=book
with open("booksFinal.txt", "wb") as fp:   #Pickling
    pickle.dump(bookDict, fp)
        
