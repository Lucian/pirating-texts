'''
Created on Jun 22, 2018

@author: Lucian
'''
import csv
weights=[]
with open('similarity6.csv', 'r') as inp, open('second_edit.csv', 'w') as out:
    writer = csv.writer(out,lineterminator = '\n')
    for row in csv.reader(inp):
        
        if row[0]+row[1] not in weights:
            writer.writerow(row)
            
        else:
            print(row)
        weights.append(row[1]+row[0])            