'''
Created on Jun 13, 2018

@author: Lucian
'''
import math
from textblob import TextBlob as tb
from book import book
import pickle
import nltk
from nltk import ne_chunk, pos_tag, word_tokenize

bookList=[]
blobList=[]
with open("books.txt", "rb") as fp:
        bookList=pickle.load(fp)
def tf(word, blob):
    return blob.words.count(word) / len(blob.words)

def n_containing(word, bloblist):
    return sum(1 for blob in bloblist if word in blob.words)

def idf(word, bloblist):
    return math.log(len(bloblist) / (1 + n_containing(word, bloblist)))

def tfidf(word, blob, bloblist):
    return tf(word, blob) * idf(word, bloblist)
for x in range(0,5):
    blobList.append(tb(bookList[x].text))
for i, blob in enumerate(blobList):
    print("Top words in document {}".format(i + 1))
    scores = {word: tfidf(word, blob, blobList) for word in blob.words}
    sorted_words = sorted(scores.items(), key=lambda x: x[1], reverse=True)
    for word, score in sorted_words[:3]:
        print("\tWord: {}, TF-IDF: {}".format(word, round(score, 5)))
'''
for i, book in enumerate(bookList):
    
    tokens=nltk.word_tokenize(book.text)
    print("Top words in {}".format(book.title))
    scores = {word: tfidf(word, book.text, bookList) for word in tokens}
    sorted_words = sorted(scores.items(), key=lambda x: x[1], reverse=True)
    for word, score in sorted_words[:3]:
        print("\tWord: {}, TF-IDF: {}".format(word, round(score, 5)))
'''