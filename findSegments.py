'''
Created on Jun 18, 2018

@author: Lucian
'''
import pickle
from fuzzywuzzy import fuzz, process
import nltk
from nltk import ne_chunk, pos_tag, word_tokenize, sent_tokenize
from book import book

count=0
bookList=[]
newList=[]
contains=False
with open("books.txt", "rb") as fp:
        bookList=pickle.load(fp)
for stuff in bookList:
    newBook=book(stuff.title, stuff.date, stuff.author, stuff.language, stuff.code, stuff.text)
    contains=False
    print(stuff.title)
    text=stuff.text
    sents=sent_tokenize(text)
    for sent in sents:
        if len(sent)>10:
            if fuzz.token_set_ratio(sent, "print foot")>80:
                newBook.hasFoot=True
                contains=True
            if fuzz.partial_ratio(sent, "island of despair")>80:
                newBook.hasJournal=True
            if fuzz.partial_ratio(sent, "horrible desolate island")>80:
                newBook.hasEvilGood=True
            if fuzz.partial_ratio(sent, "beat much for all that")>80:
            
                newBook.hasMaster=True
            if fuzz.partial_ratio(sent, "emperor of morocco")>80:
                
                newBook.hasMorocco=True 
            if fuzz.partial_ratio(sent, "Sallee")>80:
                
                newBook.hasMorocco=True
            if fuzz.partial_ratio(sent, "prayed to God")>80:
                newBook.hasDream=True
            if fuzz.partial_ratio(sent, "tedious and difficult")>80:
                print (sent)
                
                newBook.hasSpain=True                 
    if contains==True:
        count+=1
    newList.append(newBook)
#island of despair, horrible desolate island, 
print(count)
with open("books.txt", "wb") as fp:   #Pickling
        pickle.dump(newList, fp)