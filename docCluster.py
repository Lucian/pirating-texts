'''
Created on Jun 13, 2018

@author: Lucian
'''

from __future__ import print_function

import numpy as np
import pandas as pd
from pandas import DataFrame
import nltk
import re
import os
import codecs
from sklearn import feature_extraction
import mpld3
import pickle
from nltk.stem.snowball import SnowballStemmer
from sklearn.externals import joblib
import os  # for os.path.basename
import string
import matplotlib.pyplot as plt
import matplotlib as mpl
from book import book
from sklearn.manifold import MDS
from book import book

#some ipython magic to show the matplotlib plots inline
#%matplotlib inline 
terms=[]
bookList=[]
with open("bookAbridged.txt", "rb") as fp:
        bookList=pickle.load(fp)

with open('canonical.txt', 'r', encoding='utf-8', errors='ignore') as myfile:
    text = myfile.read()
whiteList=string.ascii_letters+ string.digits+ " "+"."+"\'"
text = ''.join(c if c in whiteList else " " for c in text )
canonical=book("Canonical", "Canonical", "Canonical", "English", "Canonical", text)
canonical.hasEvilGood=True
canonical.hasFoot=True
canonical.hasJournal=True
canonical.hasMaster=True
bookList.append(canonical)
textList=[]
dateList=[]
titleList=[]
lens=[]
codes=[]
scenes=[]
for book in bookList:
    dateList.append(book.date)
    textList.append(book.text)
    titleList.append(book.title)
    lens.append(len(book.text))
    codes.append(book.code)
    scenes.append((book.hasEvilGood,book.hasJournal,book.hasFoot,book.hasMaster,book.hasMorocco,book.hasDream))
    
stopwords = nltk.corpus.stopwords.words('english')
stemmer=SnowballStemmer("english")


def tokenize_and_stem(text):
    # first tokenize by sentence, then by word to ensure that punctuation is caught as it's own token
    tokens = [word for sent in nltk.sent_tokenize(text) for word in nltk.word_tokenize(sent)]
    filtered_tokens = []
    # filter out any tokens not containing letters (e.g., numeric tokens, raw punctuation)
    for token in tokens:
        if len(token)<3:
            tokens.remove(token)
            continue
        if "fay" in token:
            token="say"
        if "fee" in token:
            token="see"
        if "fo" in token:
            token="so"
        if "ft" in token:
            token="st"
        if "\'d" in token:
            token=""
       
        if re.search('[a-zA-Z]', token):
            filtered_tokens.append(token)
        
    stems = [stemmer.stem(t) for t in filtered_tokens]
    
    return stems


def tokenize_only(text):
    # first tokenize by sentence, then by word to ensure that punctuation is caught as it's own token
    tokens = [word.lower() for sent in nltk.sent_tokenize(text) for word in nltk.word_tokenize(sent)]
    filtered_tokens = []
    # filter out any tokens not containing letters (e.g., numeric tokens, raw punctuation)
    for token in tokens:
        if re.search('[a-zA-Z]', token):
            filtered_tokens.append(token)
    return filtered_tokens
#not super pythonic, no, not at all.
#use extend so it's a big flat list of vocab
totalvocab_stemmed = []
totalvocab_tokenized = []
'''
for i in bookList:
    print(i.title)
    
    allwords_stemmed = tokenize_and_stem(i.text) #for each item in 'synopses', tokenize/stem
    totalvocab_stemmed.extend(allwords_stemmed) #extend the 'totalvocab_stemmed' list
    allwords_tokenized = tokenize_only(i.text)
    totalvocab_tokenized.extend(allwords_tokenized)
    print(len(totalvocab_stemmed))
    print(len(totalvocab_tokenized))

vocab_frame = pd.DataFrame({'words': totalvocab_tokenized}, index = totalvocab_stemmed)

print ('there are ' + str(vocab_frame.shape[0]) + ' items in vocab_frame')
'''

'''
from sklearn.feature_extraction.text import TfidfVectorizer

#define vectorizer parameters

tfidf_vectorizer = TfidfVectorizer(max_df=0.8, max_features=200000,
                                 min_df=0.2, stop_words='english',
                                 use_idf=True, tokenizer=tokenize_and_stem, ngram_range=(1,3))

tfidf_matrix = tfidf_vectorizer.fit_transform(textList) #fit the vectorizer to synopses

print(tfidf_matrix.shape)

terms = tfidf_vectorizer.get_feature_names()
with open("terms.txt", "wb") as fp:   #Pickling
        pickle.dump(terms, fp)
from sklearn.metrics.pairwise import cosine_similarity
dist = 1 - cosine_similarity(tfidf_matrix)
with open("dist.txt", "wb") as fp:   #Pickling
        pickle.dump(dist, fp)
'''
from sklearn.cluster import KMeans

num_clusters = 5

km = KMeans(n_clusters=num_clusters)

##km.fit(tfidf_matrix)

##clusters = km.labels_.tolist()


#uncomment the below to save your model 
#since I've already run my model I am loading from the pickle

##joblib.dump(km,  'doc_cluster.pkl')

km = joblib.load('doc_cluster.pkl')
clusters = km.labels_.tolist()


books = { 'title': titleList, 'date': dateList, 'text': textList, 'cluster': clusters, 'length':lens, 'code':codes, 'scenes':scenes}
frame = pd.DataFrame(books, index = [clusters] , columns = ['date', 'title', 'cluster','length','code','scenes'])
frame['cluster'].value_counts() #number of films per cluster (clusters from 0 to 4)
grouped = frame['scenes'].groupby(frame['cluster']) #groupby cluster for aggregation purposes

#grouped.mean() #average rank (1 to 100) per cluster

with open("terms.txt", "rb") as fp:
        terms=pickle.load(fp)
with open("dist.txt", "rb") as fp:
        dist=pickle.load(fp)

print("Top terms per cluster:")
print()
#sort cluster centers by proximity to centroid
order_centroids = km.cluster_centers_.argsort()[:, ::-1] 

for i in range(num_clusters):
    print("Cluster %d words:" % i, end='')
    
    for ind in order_centroids[i, :6]: #replace 6 with n words per cluster
        print(terms[ind].split(' '))
    print() #add whitespace
    print() #add whitespace
    
    print("Cluster %d titles:" % i, end='')
    for date in frame.ix[i]['code'].values.tolist():
        print(' %s,' % date, end='')
    print() #add whitespace
    print() #add whitespace
    
    
print()
print()

MDS()

# convert two components as we're plotting points in a two-dimensional plane
# "precomputed" because we provide a distance matrix
# we will also specify `random_state` so the plot is reproducible.
mds = MDS(n_components=2, dissimilarity="precomputed", random_state=1)

pos = mds.fit_transform(dist)  # shape (n_components, n_samples)

xs, ys = pos[:, 0], pos[:, 1]
print()
print()

#set up colors per clusters using a dict
cluster_colors = {0: '#1b9e77', 1: '#d95f02', 2: '#7570b3', 3: '#e7298a', 4: '#66a61e'}

#set up cluster names using a dict
cluster_names = {0: 'elf, inde, fir', 
                 1: 'present, observ, viz', 
                 2: 'xuri, resolv, howev', 
                 3: 'themselv, inde, occas', 
                 4: 'vessel, ashor, succeed'}



#create data frame that has the result of the MDS plus the cluster numbers and titles
df = pd.DataFrame(dict(x=xs, y=ys, label=clusters, title=scenes)) 

#group by cluster
groups = df.groupby('label')


# set up plot
fig, ax = plt.subplots(figsize=(17, 9)) # set size
ax.margins(0.05) # Optional, just adds 5% padding to the autoscaling

#iterate through groups to layer the plot
#note that I use the cluster_name and cluster_color dicts with the 'name' lookup to return the appropriate color/label
for name, group in groups:
    ax.plot(group.x, group.y, marker='o', linestyle='', ms=12, 
            label=cluster_names[name], color=cluster_colors[name], 
            mec='none')
    ax.set_aspect('auto')
    ax.tick_params(\
        axis= 'x',          # changes apply to the x-axis
        which='both',      # both major and minor ticks are affected
        bottom='off',      # ticks along the bottom edge are off
        top='off',         # ticks along the top edge are off
        labelbottom='off')
    ax.tick_params(\
        axis= 'y',         # changes apply to the y-axis
        which='both',      # both major and minor ticks are affected
        left='off',      # ticks along the bottom edge are off
        top='off',         # ticks along the top edge are off
        labelleft='off')
    
ax.legend(numpoints=1)  #show legend with only 1 point

#add label in x,y position with the label as the film title
for i in range(len(df)):
    ax.text(df.ix[i]['x'], df.ix[i]['y'], df.ix[i]['title'], size=8)  

    
    
plt.show() #show the plot

#uncomment the below to save the plot if need be
#plt.savefig('clusters_small_noaxes.png', dpi=200)
